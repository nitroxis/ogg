﻿using System.IO;

namespace Ogg
{
	/// <summary>
	/// Represents a stream that computes a CRC checksum of the data that passes through.
	/// </summary>
	internal sealed class Crc32Stream : Stream
	{
		#region Fields

		private readonly Stream baseStream;
		private uint crc32;

		#endregion

		#region Properties

		public Stream BaseStream
		{
			get { return this.baseStream; }
		}
		
		public override bool CanRead
		{
			get { return this.baseStream.CanRead; }
		}

		public override bool CanWrite
		{
			get { return this.baseStream.CanWrite; }
		}
		
		public override bool CanSeek
		{
			get { return this.baseStream.CanSeek; }
		}

		public override long Length
		{
			get { return this.baseStream.Length; }
		}

		public override long Position
		{
			get { return this.baseStream.Position; }
			set { this.baseStream.Position = value; }
		}

		public uint Crc32
		{
			get { return this.crc32; }
			set { this.crc32 = value; }
		}

		#endregion

		#region Constructors

		public Crc32Stream(Stream baseStream)
		{
			this.baseStream = baseStream;
		}

		#endregion

		#region Methods

		public override int ReadByte()
		{
			int value = this.baseStream.ReadByte();
			if (value >= 0)
				this.crc32 = ((this.crc32 << 8) ^ Crc.Crc32Table[value ^ (this.crc32 >> 24)]);

			return value;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int n = this.baseStream.Read(buffer, offset, count);
			
			for (int i = 0; i < n; i++)
				this.crc32 = ((this.crc32 << 8) ^ Crc.Crc32Table[buffer[offset + i] ^ (this.crc32 >> 24)]);

			return n;
		}

		public override void WriteByte(byte value)
		{
			this.crc32 = ((this.crc32 << 8) ^ Crc.Crc32Table[value ^ (this.crc32 >> 24)]);

			this.baseStream.WriteByte(value);
		}
		
		public override void Write(byte[] buffer, int offset, int count)
		{
			for (int i = 0; i < count; i++)
				this.crc32 = ((this.crc32 << 8) ^ Crc.Crc32Table[buffer[offset + i] ^ (this.crc32 >> 24)]);

			this.baseStream.Write(buffer, offset, count);
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.baseStream.Seek(offset, origin);
		}

		public override void Flush()
		{
			this.baseStream.Flush();
		}

		public override void SetLength(long value)
		{
			this.baseStream.SetLength(value);
		}

		#endregion
	}
}
