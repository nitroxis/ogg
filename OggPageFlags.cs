﻿using System;

namespace Ogg
{
	/// <summary>
	/// The header type flag identifies a page's context in the bitstream.
	/// </summary>
	[Flags]
	public enum OggPageFlags
	{
		ContinuedPacket = 0x01,
		FirstPage = 0x02,
		LastPage = 0x04,
	}
}
