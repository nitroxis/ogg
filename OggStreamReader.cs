﻿using System;
using System.IO;

namespace Ogg
{
	/// <summary>
	/// Represents a reader for Ogg streams.
	/// </summary>
	public sealed class OggStreamReader : Stream
	{
		#region Fields

		private static readonly byte[] signature = {0x4F, 0x67, 0x67, 0x53};
		private static readonly uint signatureCrc32 = Crc.Crc32(0, signature, 0, 4);
		
		private readonly Crc32Stream stream;
		
		private OggPageFlags pageFlags;
		private ulong absoluteGranulePosition;
		private uint serialNumber;
		private uint pageSequenceNumber;
		private uint pageChecksum;
		private int numSegments;
		private readonly int[] segmentTable;
		private int pageSize;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the current page's flags.
		/// </summary>
		public OggPageFlags PageFlags
		{
			get { return this.pageFlags; }
		}

		/// <summary>
		/// Gets the current page's absolute granule position.
		/// </summary>
		public ulong AbsoluteGranulePosition
		{
			get { return this.absoluteGranulePosition; }
		}

		/// <summary>
		/// Gets the current page's serial number.
		/// </summary>
		public uint SerialNumber
		{
			get { return this.serialNumber; }
		}

		/// <summary>
		/// Gets the current page's sequence number.
		/// </summary>
		public uint PageSequenceNumber
		{
			get { return this.pageSequenceNumber; }
		}

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanWrite
		{
			get { return false; }
		}

		public override bool CanSeek
		{
			get { return this.stream.CanSeek; }
		}

		public override long Length
		{
			get { return this.stream.Length; }
		}

		public override long Position
		{
			get { return this.stream.Position; }
			set { this.stream.Position = value; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new OggReader.
		/// </summary>
		public OggStreamReader(Stream stream)
		{
			this.stream = new Crc32Stream(stream);
			this.segmentTable = new int[256];
			this.pageSize = -1;
		}

		#endregion

		#region Methods

		/// <summary>
		/// Searches for the "OggS" byte sequence.
		/// </summary>
		/// <returns>True, if the sequence has been found, false if the stream ended.</returns>
		private bool findSignature()
		{
			int i = 0;
			while (true)
			{
				int c = this.stream.ReadByte();
				if (c < 0)
					return false;

				if (c == signature[i])
				{
					if (++i == 4)
						return true;
				}
				else
				{
					i = 0;
				}
			}
		}

		/// <summary>
		/// Reads the next page header. Skips all data in between, if there is any.
		/// </summary>
		/// <returns>True, if the page header has been read, false if the stream ended.</returns>
		public bool ReadPageHeader()
		{
			if (!this.findSignature())
				return false;

			// the signature (OggS) has already been read, so we need to reconstruct the CRC-32 resulting from that.
			this.stream.Crc32 = signatureCrc32;
			
			int version = this.stream.ReadByte();
			if (version < 0)
				throw new EndOfStreamException();

			if (version != 0)
				throw new InvalidDataException("Invalid stream structure version number.");

			this.pageFlags = (OggPageFlags)this.stream.ReadByte();
			if (this.pageFlags < 0)
				throw new EndOfStreamException();

			this.absoluteGranulePosition = readUInt64(this.stream);
			this.serialNumber = readUInt32(this.stream);
			this.pageSequenceNumber = readUInt32(this.stream);
			this.pageChecksum = readUInt32(this.stream.BaseStream);
			this.stream.Crc32 = Crc.Crc32(this.stream.Crc32, new byte[] {0, 0, 0, 0}, 0, 4);
			
			this.numSegments = this.stream.ReadByte();
			if (numSegments < 0)
				throw new EndOfStreamException();

			this.pageSize = 0;
			for (int i = 0; i < numSegments; i++)
			{
				this.segmentTable[i] = this.stream.ReadByte();
				if (this.segmentTable[i] < 0)
					throw new EndOfStreamException();
				this.pageSize += this.segmentTable[i];
			}

			return true;
		}

		private bool nextPage()
		{
			// check CRC-32.
			if (this.stream.Crc32 != this.pageChecksum)
				throw new InvalidDataException("CRC-32 check failed. The stream is corrupted.");

			// read the next page to see if it continues the current packet.
			return this.ReadPageHeader();
		}

		/// <summary>
		/// Reads data from the page's contents.
		/// </summary>
		/// <param name="buffer"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public override int Read(byte[] buffer, int offset, int count)
		{
			// first page in the stream.
			if (this.pageSize < 0)
			{
				if (!this.ReadPageHeader())
					return 0;
			}

			int totalBytes = 0;
			while (totalBytes < count)
			{
				int numBytes = this.stream.Read(buffer, offset + totalBytes, Math.Min(count - totalBytes, this.pageSize));
				this.pageSize -= numBytes;
				totalBytes += numBytes;

				// we reached the end of the current page.
				if (this.pageSize == 0)
				{
					if (!this.nextPage())
						break;

					// if it does not continue the packet, bail. 
					// this way the application can check the new page parameters.
					if (!this.pageFlags.HasFlag(OggPageFlags.ContinuedPacket))
						break;
				}
			}

			return totalBytes;
		}

		public override int ReadByte()
		{
			// first page in the stream.
			if (this.pageSize < 0)
			{
				if (!this.ReadPageHeader())
					return 0;
			}

			int b = this.stream.ReadByte();
			if (--this.pageSize == 0)
				this.nextPage();
			
			return b;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			this.pageSize = 0;
			return this.stream.Seek(offset, origin);
		}

		public override void Flush()
		{
			this.stream.Flush();
		}

		private static ulong readUInt64(Stream s)
		{
			ulong value = 0;
			for (int i = 0; i < 8; i++)
			{
				int b = s.ReadByte();
				if (b < 0)
					throw new EndOfStreamException();

				value |= ((ulong)b << (i * 8));
			}
			return value;
		}

		private static uint readUInt32(Stream s)
		{
			uint value = 0;
			for (int i = 0; i < 4; i++)
			{
				int b = s.ReadByte();
				if (b < 0)
					throw new EndOfStreamException();

				value |= ((uint)b << (i * 8));
			}
			return value;
		}


		#endregion
	}
}
